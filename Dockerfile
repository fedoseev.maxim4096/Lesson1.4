FROM debian:9 as stage1st

RUN apt-get update && apt-get upgrade && apt install -y wget gcc make libpcre3 libpcre3-dev zlib1g zlib1g-dev
RUN wget http://nginx.org/download/nginx-1.21.3.tar.gz && tar xzvf nginx-1.21.3.tar.gz && cd nginx-1.21.3 && ./configure && make && make install

FROM debian:9 as stage2nd
WORKDIR /usr/local/nginx
RUN mkdir logs && touch ../error.log && touch ../access.log && touch ../nginx.pid
WORKDIR /usr/local/nginx/sbin
COPY --from=stage1st /usr/local/nginx/sbin/nginx .
RUN chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
